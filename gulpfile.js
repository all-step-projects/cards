import gulp from "gulp";
import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import fileInclude from "gulp-file-include";
import bsc from "browser-sync";
import * as rollup from "rollup";
import multiEntry from "rollup-plugin-multi-entry";

const { src, dest, watch, series, parallel } = gulp;
const sass = gulpSass(dartSass);
const browserSync = bsc.create();

const htmlTaskHandler = () => {
  return src("./src/*.html").
  pipe(fileInclude()).
  pipe(dest("./dist"));
};

const cssTaskHandler = () => {
  return src("./src/scss/main.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(csso())
    .pipe(dest("./dist/styles"))
    .pipe(browserSync.stream());
};

const imagesTaskHandler = () => {
  return src("./src/images/**/*.*")
    .pipe(imagemin())
    .pipe(dest("./dist/images"));
};

const fontTaskHandler = () => {
  return src("./src/fonts/**/*.*")
  .pipe(dest("./dist/fonts"));
};

const jsTaskHandler = async () => {
  const bundle = await rollup.rollup({
    input: "./src/scripts/**/*.js",
    plugins: [multiEntry()],
  });

  await bundle.write({
    file: "./dist/scripts/index.js",
    format: "esm",
  });
};

const cleanDistTaskHandler = () => {
  return src("./dist", { read: false, allowEmpty: true }).pipe(
    clean({ force: true })
  );
};

const browserSyncTaskHandler = () => {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
  });

  watch("./src/scss/**/*.scss").on(
    "all",
    series(cssTaskHandler, browserSync.reload)
  );

  watch("./src/**/*.html").on(
    "change",
    series(htmlTaskHandler, browserSync.reload)
  );

  watch("./src/images/**/*").on(
    "all",
    series(imagesTaskHandler, browserSync.reload)
  );

  watch("./src/scripts/**/*.js").on(
    "all",
    series(jsTaskHandler, browserSync.reload)
  );
};

export const cleaning = cleanDistTaskHandler;
export const html = htmlTaskHandler;
export const css = cssTaskHandler;
export const font = fontTaskHandler;
export const images = imagesTaskHandler;
export const js = jsTaskHandler;

export const build = series(
  cleanDistTaskHandler,
  parallel(
    htmlTaskHandler,
    cssTaskHandler,
    fontTaskHandler,
    imagesTaskHandler,
    jsTaskHandler
  )
);
export const dev = series(build, browserSyncTaskHandler);
