import { TOKEN } from "./consts.js";

// METHOD GET: get all cards from the database:
export const CONFIG_GET_ALL = {
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${TOKEN}`,
  },
};

// METHOD POST: send card into the database
export function CONFIG_POST(data) {
  return {
    headers: {
      Authorization: `Bearer ${TOKEN}`,
    },
    body: JSON.stringify(Object.fromEntries(data.entries())),
  };
}

// METHOD PUT: send card into the database
export function CONFIG_PUT(data) {
  return {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${TOKEN}`,
    },
    body: JSON.stringify(Object.fromEntries(data.entries())),
  };
}

// METHOD DELETE: remove one card from database by Id:
export const CONFIG_DEL = {
  headers: {
    Authorization: `Bearer ${TOKEN}`,
  },
};
