// Select options
import { BODY } from "../configs/consts.js";
export let select = function () {
    const selectHeader = document.querySelectorAll('.select__header');
    const selectItem = document.querySelectorAll('.select__item');
    const selectBlocks = document.querySelectorAll('.select');

    BODY.addEventListener('keyup', (event) => {
        selectBlocks.forEach((item) => {
            if (event.code === 'Escape' && item.classList.contains('is-active')) {
                item.classList.remove('is-active');
            }
        });
    });

    BODY.addEventListener('click', (event) => {
        selectBlocks.forEach((element) => {
            if(!event.target.closest('.select')){
                element.classList.remove('is-active');
            }
        })
    });

    selectHeader.forEach((item) => {
        item.addEventListener('click', selectToggle);
    });

    selectItem.forEach(item => {
        item.addEventListener('click', selectChoose)
    });

    function selectToggle() {
        this.parentElement.classList.toggle('is-active');
    }

    function selectChoose() {
        let text = this.innerText,
            select = this.closest('.select'),
            currentText = select.querySelector('.select__current');
        currentText.innerText = text;
        select.classList.remove('is-active');
    }
};

select();