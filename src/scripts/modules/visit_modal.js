import { VisitFormBase } from "./visit_Base.js";
import { VisitDentist } from "./visit_Dentist.js";
import { VisitCardiologist } from "./visit_Cardiologist.js";
import { VisitTherapist } from "./visit_Therapist.js";

// клас модального вікна для форми
export class ModalVisitFormRender {
    constructor() {
        this.choiceDoctor = document.getElementById("modal-visit__dropdownDoctor"); // поле вибору лікаря        
        this.fieldsContainers = document.querySelectorAll(".fields_container"); // всі контейнери з полями для заповнення
        this.createVisitFormWrapper = document.getElementById("modal-visit__form-wrapper"); // обгортка форми, покриває весь екран абсолютом
        this.btnCreateVisit = document.getElementById("btn_create_visit");

        this.btnCloseCreateVisit = document.getElementById("modal-visit__btn-close");
        this.visitForm = document.getElementById("modal-visit__create"); // форма

        this.doctorType = null; // Поле для зберігання типу лікаря
        this.visitFormBase = new VisitFormBase(); // Створюємо об'єкт VisitFormBase
        this.visitDentist = new VisitDentist(); // Створюємо об'єкт VisitDentist
        this.visitCardiologist = new VisitCardiologist(); // Створюємо об'єкт VisitCardiologist
        this.visitTherapist = new VisitTherapist(); // Створюємо об'єкт VisitTherapist    

        this.setupEventListener();
    }

    // метод для відкриття модального вікна для творення картки візиту
    openModalCreateVisitForm() {
        this.createVisitFormWrapper.style.display = "block";
        this.resetForm(); // Скидання форми до початкового стану
    } 

    // метод для відкриття модального вікна для редагування існуючої картки візиту
    openModalEditVisitFormWithSelectedDoctor(specialistValue) {
        this.createVisitFormWrapper.style.display = "block";
        this.choiceDoctor.value = specialistValue; // Встановлення типу лікаря в поле вибору лікаря, щоб обійти початковий вибір

        if (specialistValue === "dentist") {
            this.visitDentist.showEditForm(); // Викликаємо метод для відображення форми дантиста
        } else if (specialistValue === "cardiologist") {
            this.visitCardiologist.showEditForm(); // Викликаємо метод для відображення форми кардіолога
        } else if (specialistValue === "therapist") {
            this.visitTherapist.showEditForm(); // Викликаємо метод для відображення форми терапевта
        } else {
            this.visitFormBase.showEditForm(); // Викликаємо метод для відображення форми for instance, Michael Jackson - тимчаова версія
        }
    }

    // метод для закриття модального вікна
    closeModalCreateVisitForm() {
        this.createVisitFormWrapper.style.display = "none";
    }

    // Метод для скидання значень полів форми до початкового стану
    resetForm() {
        this.visitForm.reset(); // Скидання всіх полів форми до початкового стану
        this.fieldsContainers.forEach(container => {
            container.setAttribute("data-show", "hidden"); // Сховати всі контейнери полів, крім контейнера з полем вибору лікаря
        });
        this.choiceDoctor.selectedIndex = 0; // Скидання вибору лікаря до початкового значення
    }

    // Обробники подій для відкриття та закриття модального вікна
    setupEventListener() {
        this.btnCreateVisit.addEventListener("click", () => this.openModalCreateVisitForm());
        this.btnCloseCreateVisit.addEventListener("click", () => this.closeModalCreateVisitForm());
        this.visitForm.addEventListener("submit", (event) => {
            event.preventDefault();
            this.closeModalCreateVisitForm();
        })
        // обробник події закриття форми при натисканні миші за її межами
        this.createVisitFormWrapper.addEventListener("mousedown", (event) => {
            if (!this.visitForm.contains(event.target)) {
                this.closeModalCreateVisitForm();
            }
        });
        // обробник події закриття форми при натисканні клавіши Esc
        document.addEventListener("keydown", (event) => {
            if (event.key === "Escape") {
                this.closeModalCreateVisitForm();
            }
        })
        // обробник події відправки форми (кнопка Create Card) при натисканні keydown клавіши Enter
        const submitButton = document.getElementById("btn_submit_form");
        submitButton.addEventListener("keydown", (event) => {
            if (event.key === "Enter") {
                event.preventDefault() // Забороняємо дійсну дію, щоб уникнути події "submit", не зрозуміла необхідність
                submitButton.click(); // Симулюємо клік по кнопці відправки форми
            }
        })
    }
}

const modalVisitFormRender = new ModalVisitFormRender();