// Class for create medical cards
export class Cards {
  // Manage start text "No items have been added"
  static switchTextEmptyCards() {
    const cardsExist = document.querySelector(".card");
    const textEmptyCards = document.querySelector("#text-empty-cards");

    if (cardsExist) {
      textEmptyCards.style.display = "none";
    } else {
      textEmptyCards.style.display = "block";
    }
  }

  // Cwitch between Show More / Show Less for cards
  static switchCards() {
    const cardFooter = this.createContainerCard.querySelector(".card_footer");
    const btnShowMore = cardFooter.querySelector(".btn_show_more");
    const btnShowLess = cardFooter.querySelector(".btn_show_less");
    const cardExpand = this.createContainerCard.querySelector(".card_expand");
    const cardClass = cardExpand.closest(".card");

    // Actions for Show More
    btnShowMore.addEventListener("click", () => {
      btnShowMore.style.display = "none"; // Hide button "Show More";
      btnShowLess.style.display = "inline-block"; // Show button "Show Less";
      cardExpand.style.display = "block"; // Show expand contant;
      cardClass.classList.add("grid-expand"); // Add class that expand Grid body card;
    });

    // Actions for Show Less
    btnShowLess.addEventListener("click", () => {
      btnShowMore.style.display = "inline-block"; // Show button "Show More";
      btnShowLess.style.display = "none"; // Hide button "Show Less";
      cardExpand.style.display = "none"; // Show expand contant;
      cardClass.classList.remove("grid-expand"); // Remove class that expand Grid body card;
    });
  }

  static getCardData(item) {
    // HEADER
    this.firstName = item.firstName;
    // this.lastName = item.lastName;
    this.lastName = item.lastName.toUpperCase();
    this.patronymic = item.patronymic;
    this.userName = item.fullName; // Перевірити на використання та видалити
    this.userId = item.id;

    // BASE
    // this.specialist = item.dropdownDoctor;
    this.specialist = item.dropdownDoctor.toUpperCase();
    this.complaint = item.title;
    this.priority = item.dropdownUrgency;
    this.status = item.status;

    // EXPAND
    this.age = item.age ? item.age : "";
    this.bodyHeight = item.bodyHeight ? item.bodyHeight : "";
    this.bodyWeight = item.bodyWeight ? item.bodyWeight : "";
    this.bodyMassIndex = item.bodyMassIndex ? item.bodyMassIndex : "";
    this.dateLastVisit = item.dateLastVisit ? item.dateLastVisit : "";
    this.description = item.description;
    this.normalPressure = item.normalPressure ? item.normalPressure : "";
    this.transferredCardiacDiseases = item.transferredCardiacDiseases
      ? item.transferredCardiacDiseases
      : "";
  }

  static createCardElement() {
    const boardCards = document.querySelector("#board_cards"); // Get place in HTML where will append-ing created element
    this.createContainerCard = document.createElement("div"); // Create the container for the card
    this.createContainerCard.classList.add("card", "border", "shadow"); // Add a card necessary HTML-class to our container
    this.createContainerCard.dataset.idCard = this.userId; // Add a card data-attribute to our container

    // Add all HTML-code of the card to our container
    if (this.specialist.toLowerCase() === "cardiologist") {
      this.createContainerCard.insertAdjacentHTML(
        "beforeend",
        `
      <button class="btn__del btn_delete_card"></button>
      <button class="btn__edit btn_edit_card"></button>
      <div class="card__wrap">
        <div class="card__header">
          <p class="card__header-number">№ ${this.userId}</p>
          <p class="card_header-name-last-name card_lastName">${this.lastName}</p>
          <p class="card__header-username card_firstName">${this.firstName} ${this.patronymic}</p>
        </div>

        <div class="card__body">
          <div class="card__base">
              <p class="card__patient card_specialist"><span class="bold">To a Specialist: </span><span class="card_specialist_id">${this.specialist}</span></p>
              <p class="card__patient card_complaint"><span class="bold">Complaint: </span> ${this.complaint}</p>
              <p class="card__patient card_priority">
                <span class="bold">Priority:</span> ${this.priority}
              </p>
              <p class="card__patient card_status"><span class="bold">Status:</span> ${this.status}</p>
          </div>
          
          <div class="card__expand card_expand" style="display: none">
              <p class="card__patient card_age"><span class="bold">Age:</span> ${this.age}</p>
              <p class="card__patient card_bodyHeight">
                  <span class="bold">Body Height:</span> ${this.bodyHeight}</p>
              <p class="card__patient card_bodyWeight"><span class="bold">Body Weight:</span> ${this.bodyWeight}</p>
              <p class="card__patient card_bodyMassIndex">
                  <span class="bold">Body Mass Index:</span> ${this.bodyMassIndex}
              </p>              
              <p class="card__patient card_description"><span class="bold">Description:</span> ${this.description}</p>
              <p class="card__patient card_normalPressure">
                  <span class="bold">Normal Pressure:</span> ${this.normalPressure}
              </p>
              <p class="card__patient card_transferredCardiacDiseases"><span class="bold">Transferred Cardiac Diseases:</span> ${this.transferredCardiacDiseases}</p>
          </div>
        </div>
        <div class="card__footer card_footer">
          <button class="btn card__btn btn_show_more">Show more</button>
          <button class="btn card__btn btn_show_less" style="display: none">Show less</button>
        </div>
      </div>
          `
      );
    } else if (this.specialist.toLowerCase() === "dentist") {
      this.createContainerCard.insertAdjacentHTML(
        "beforeend",
        `
      <button class="btn__del btn_delete_card"></button>
      <button class="btn__edit btn_edit_card"></button>
      <div class="card__wrap">
        <div class="card__header">
          <p class="card__header-number">№ ${this.userId}</p>
          <p class="card_header-name-last-name card_lastName">${this.lastName}</p>
          <p class="card__header-username card_firstName">${this.firstName} ${this.patronymic}</p>
        </div>

        <div class="card__body">
          <div class="card__base">
              <p class="card__patient card_specialist"><span class="bold">To a Specialist: </span><span class="card_specialist_id">${this.specialist}</span></p>
              <p class="card__patient card_complaint"><span class="bold">Complaint: </span> ${this.complaint}</p>
              <p class="card__patient card_priority">
                <span class="bold">Priority:</span> ${this.priority}
              </p>
              <p class="card__patient card_status"><span class="bold">Status:</span> ${this.status}</p>
          </div>
          
          <div class="card__expand card_expand" style="display: none">
              <p class="card__patient card_description"><span class="bold">Description:</span> ${this.description}</p>
              <p class="card__patient card_dateLastVisit">
                  <span class="bold">The Date of Last Visit:</span> ${this.dateLastVisit}
              </p>
          </div>
        </div>
        <div class="card__footer card_footer">
          <button class="btn card__btn btn_show_more">Show more</button>
          <button class="btn card__btn btn_show_less" style="display: none">Show less</button>
        </div>
      </div>
          `
      );
    } else {
      this.createContainerCard.insertAdjacentHTML(
        "beforeend",
        `
      <button class="btn__del btn_delete_card"></button>
      <button class="btn__edit btn_edit_card"></button>
      <div class="card__wrap">
        <div class="card__header">
          <p class="card__header-number">№ ${this.userId}</p>
          <p class="card_header-name-last-name card_lastName">${this.lastName}</p>
          <p class="card__header-username card_firstName">${this.firstName} ${this.patronymic}</p>
        </div>

        <div class="card__body">
          <div class="card__base">
              <p class="card__patient card_specialist"><span class="bold">To a Specialist: </span><span class="card_specialist_id">${this.specialist}</span></p>
              <p class="card__patient card_complaint"><span class="bold">Complaint: </span> ${this.complaint}</p>
              <p class="card__patient card_priority">
                <span class="bold">Priority:</span> ${this.priority}
              </p>
              <p class="card__patient card_status"><span class="bold">Status:</span> ${this.status}</p>
          </div>
          
          <div class="card__expand card_expand" style="display: none">
              <p class="card__patient card_age"><span class="bold">Age:</span> ${this.age}</p>
              <p class="card__patient card_description"><span class="bold">Description:</span> ${this.description}</p>
          </div>
        </div>
        <div class="card__footer card_footer">
          <button class="btn card__btn btn_show_more">Show more</button>
          <button class="btn card__btn btn_show_less" style="display: none">Show less</button>
        </div>
      </div>
          `
      );
    }

    boardCards.append(this.createContainerCard);

    Cards.switchCards();

    Cards.switchTextEmptyCards(); // Call switchTextEmptyCards() after cards have been created
  }

  static updateCardElement(cardId, newData) {
    const cardToUpdate = document.querySelector(`[data-id-card="${cardId}"]`);
    if (!cardToUpdate) {
      console.error("Card not found!");
      return;
    }

    cardToUpdate.querySelector(".card_lastName").textContent = newData.lastName;
    cardToUpdate.querySelector(".card_firstName").textContent =
      newData.firstName + " " + newData.patronymic;

    cardToUpdate.querySelector(
      ".card_specialist"
    ).innerHTML = `<span class="bold">Specialist: </span> ${newData.dropdownDoctor}`;
    cardToUpdate.querySelector(
      ".card_complaint"
    ).innerHTML = `<span class="bold">Complaint: </span> ${newData.title}`;
    cardToUpdate.querySelector(
      ".card_priority"
    ).innerHTML = `<span class="bold">Priority:</span> ${newData.dropdownUrgency}`;
    cardToUpdate.querySelector(
      ".card_status"
    ).innerHTML = `<span class="bold">Status:</span> ${newData.status}`;

    cardToUpdate.querySelector(
      ".card_status"
    ).innerHTML = `<span class="bold">Status:</span> ${newData.status}`;
  }
}

export const cards = new Cards();
