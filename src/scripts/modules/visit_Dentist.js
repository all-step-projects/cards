// Дочірній клас VisitDentist для дантиста
import { VisitFormBase } from "./visit_Base.js";

export class VisitDentist extends VisitFormBase {
    constructor() {
        super();
        this.dentistFieldsContainer = document.getElementById("dentist");
    }

    // метод для Створення Нової картки, відкриття форми з відповідними полями під клас дантиста
    onDoctorChange() {
        if (this.choiceDoctor.value === "dentist") {
            this.hideAllFieldsContainers(); // Сховати всі контейнери
            this.showCommonFields(); // Відобразити спільні поля
            this.dentistFieldsContainer.removeAttribute("data-show"); // Відобразити поля дантиста 
            this.updateDefaultFields();
            this.removeAllDisabled();  // Зняти дізейбл з усіх полів у всіх контейнерах
            this.disableNonRelevantFields();  // установити дізейбл на невалідних для певного лікаря полів
        }
    }

    // метод для Редагування Існуючої картки, відкриття форми з відповідними полями під клас дантиста
    showEditForm() {
        this.hideAllFieldsContainers(); // Сховати всі контейнери
            this.showCommonFields(); // Відобразити спільні поля
            this.dentistFieldsContainer.removeAttribute("data-show"); // Відобразити поля дантиста 
            this.updateDefaultFields();
            this.removeAllDisabled();  // Зняти дізейбл з усіх полів у всіх контейнерах
            this.disableNonRelevantFields();  // установити дізейбл на невалідних для певного лікаря полів
    }

    // Метод для оновлення значень полів за замовчуванням для зручності під час розробки
    updateDefaultFields() {
        document.getElementById("modal-visit__firstName").value = "Verka";
        document.getElementById("modal-visit__lastName").value = "Danylko";
        document.getElementById("modal-visit__patronymic").value = "Serdyuchko";
        document.getElementById("modal-visit__fullName").value = "Danylko Verka Serdyuchka";
        document.getElementById("modal-visit__title").value = "toothache";
        document.getElementById("modal-visit__description").value = "Put a filling in the tooth";
        document.getElementById("modal-visit__dropdownUrgency_dentist").value = "High";
        document.getElementById("modal-visit__dateLastVisit").value = "2023-12-01";
    }
}

const dentistVisit = new VisitDentist();