// Дочірній клас VisitCardiologist для кардіолога
import { VisitFormBase } from "./visit_Base.js";

export class VisitCardiologist extends VisitFormBase {
    constructor() {
        super();
        this.cardiologistFieldsContainer = document.getElementById("cardiologist");
        this.weightInput = document.getElementById("bodyWeight"); // вага
        this.heightInput = document.getElementById("bodyHeight"); // зріст
        this.bmiInput = document.getElementById("modal-visit__bodyMassIndex") // індекс маси тіла
        this.weightInput.addEventListener("input", this.calculateBMI.bind(this)); // обробники для розрахунку BMI (body index mass)
        this.heightInput.addEventListener("input", this.calculateBMI.bind(this));
    }

    // метод для Створення Нової картки, відкриття форми з відповідними полями під клас кардіолога
    onDoctorChange() {
        if (this.choiceDoctor.value === "cardiologist") {
            this.hideAllFieldsContainers(); // Сховати всі контейнери
            this.showCommonFields(); // Відобразити спільні поля
            this.cardiologistFieldsContainer.removeAttribute("data-show");
            this.updateDefaultFields();
            this.removeAllDisabled();  // Зняти дізейбл з усіх полів у всіх контейнерах
            this.disableNonRelevantFields();  // установити дізейбл на невалідних для певного лікаря полів
        }
    }

    // метод для Редагування Існуючої картки, відкриття форми з відповідними полями під клас кардіолога
    showEditForm() {
        this.hideAllFieldsContainers(); // Сховати всі контейнери
        this.showCommonFields(); // Відобразити спільні поля
        this.cardiologistFieldsContainer.removeAttribute("data-show"); 
        this.updateDefaultFields();
        this.removeAllDisabled();  // Зняти дізейбл з усіх полів у всіх контейнерах
        this.disableNonRelevantFields();  // установити дізейбл на невалідних для певного лікаря полів
    }

    // метод для розрахунку BMI (body max index, індекса маси тіла)
    calculateBMI() {
        const weight = parseFloat(this.weightInput.value); // вага числом з плаваючою комою
        const height = parseFloat(this.heightInput.value); // зріст в сантиметрах числом з плаваючою комою
        if (weight > 0 && height > 0) {
            const heightInMeters = height / 100;  // переводимо зріст у метри
            const bmi = weight / (heightInMeters * heightInMeters);
            this.bmiInput.value = bmi.toFixed(2); // обмежуємо 2 знаки після коми
        } else {
            this.bmiInput.value = "";
        }
    }

    // Метод для оновлення значень полів за замовчуванням для зручності під час розробки
    updateDefaultFields() {
        document.getElementById("modal-visit__firstName").value = "Taras";
        document.getElementById("modal-visit__lastName").value = "Shevchenko";
        document.getElementById("modal-visit__patronymic").value = "Hryhorovych";
        document.getElementById("modal-visit__fullName").value = "Shevchenko Taras Hryhorovych";
        document.getElementById("modal-visit__title").value = "Planned consultation";
        document.getElementById("modal-visit__description").value = "Planned consultation after surgery";
        document.getElementById("modal-visit__dropdownUrgency").value = "Low";
        document.getElementById("modal-visit__normalPressure").value = "120/80";
        document.getElementById("modal-visit__age").value = "43";
        document.getElementById("bodyWeight").value = "64";
        document.getElementById("bodyHeight").value = "170";
        document.getElementById("modal-visit__bodyMassIndex").value = "22.15";
        document.getElementById("modal-visit__transferredCardiacDiseases").value = "Implantation of a pacemaker";
    }
}

const cardiologistVisit = new VisitCardiologist();

