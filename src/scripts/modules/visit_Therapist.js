// Дочірній клас VisitTherapist для терапевта
import { VisitFormBase } from "./visit_Base.js";

export class VisitTherapist extends VisitFormBase {
    constructor() {
        super();
        this.therapistFieldsContainer = document.getElementById("therapist");
    }

    // метод для Створення Нової картки, відкриття форми з відповідними полями під клас терапевта
    onDoctorChange() {
        if (this.choiceDoctor.value === "therapist") {
            this.hideAllFieldsContainers(); // Сховати всі контейнери
            this.showCommonFields(); // Відобразити спільні поля
            this.therapistFieldsContainer.removeAttribute("data-show"); // Відобразити поля терапевта
            this.updateDefaultFields();
            this.removeAllDisabled();  // Зняти дізейбл з усіх полів у всіх контейнерах
            this.disableNonRelevantFields();   // установити дізейбл на невалідних для певного лікаря полів 
        }
    }

    // метод для Редагування Існуючої картки, відкриття форми з відповідними полями під клас кардіолога
    showEditForm() {
        this.hideAllFieldsContainers(); // Сховати всі контейнери
        this.showCommonFields(); // Відобразити спільні поля
        this.therapistFieldsContainer.removeAttribute("data-show");
        this.updateDefaultFields();
        this.removeAllDisabled();  // Зняти дізейбл з усіх полів у всіх контейнерах
        this.disableNonRelevantFields();  // установити дізейбл на невалідних для певного лікаря полів
        }
    
    // Метод для оновлення значень полів за замовчуванням для зручності під час розробки
    updateDefaultFields() {
        document.getElementById("modal-visit__firstName").value = "Lesya";
        document.getElementById("modal-visit__lastName").value = "Ukrainka";
        document.getElementById("modal-visit__patronymic").value = "ukrainian poetess";
        document.getElementById("modal-visit__fullName").value = "Ukrainka Lesya ukrainian poetess";
        document.getElementById("modal-visit__title").value = "Headache";
        document.getElementById("modal-visit__description").value = "Choose antidepressants";
        document.getElementById("modal-visit__dropdownUrgency-therapist").value = "Normal";
        document.getElementById("modal-visit__age-therapist").value = "25";
        document.getElementById("modal-visit__status-therapist").value = "done";
    }
}

const therapistVisit = new VisitTherapist();