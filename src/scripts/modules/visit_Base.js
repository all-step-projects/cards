// Базовий (батьківський) клас для роботи зі збиранням даних з форми та відправленням запиту
export class VisitFormBase {
    constructor() {
        this.formaVisitCreate = document.getElementById("modal-visit__create"); // форма створення карточки візиту
        this.choiceDoctor = document.getElementById("modal-visit__dropdownDoctor"); // поле вибору лікаря
        this.commonFieldsContainer = document.getElementById("common_fields_container"); // контейнер зі спільними полями
        this.fieldsContainers = document.querySelectorAll(".fields_container"); // всі контейнери з полями для заповнення
        this.lastNameInput = document.getElementById("modal-visit__lastName"); // прізвище
        this.firstNameInput = document.getElementById("modal-visit__firstName"); // ім'я
        this.patronymicInput = document.getElementById("modal-visit__patronymic"); // побатькові
        this.fullNameInput = document.getElementById("modal-visit__fullName"); // повне ім'я
        this.selectedDoctor = null; // Поле для збереження обраного лікаря
        this.setupEventListener(); // визиваємо цей метод-колекцію прослуховувачів у батьківському класі
    }

    // метод слідкує за подіямі на різних елементах    
    setupEventListener() {
        this.choiceDoctor.addEventListener("change", this.onDoctorChange.bind(this)); // обробники для події зміни вибора лікаря
        this.lastNameInput.addEventListener("input", this.updateFullName.bind(this)); // обробники для збірки повного імені
        this.firstNameInput.addEventListener("input", this.updateFullName.bind(this));
        this.patronymicInput.addEventListener("input", this.updateFullName.bind(this));
    }

    // метод відкриття потрібних полів, він перезаписується в дочірніх класах
    onDoctorChange() {
        // console.log("Doctor has been changed.");
    }

    // метод для Редагування Існуючої картки, відкриття форми з відповідними полями
    showEditForm() {
        this.hideAllFieldsContainers(); // Сховати всі контейнери
        this.showCommonFields(); // Відобразити спільні поля
        this.updateDefaultFields();
        }

    // ховає всі контейнери
    hideAllFieldsContainers() {
        this.fieldsContainers.forEach(container => {
            container.setAttribute("data-show", "hidden");
        });
    }

    // відкриває спільний контейнер
    showCommonFields() {
        this.commonFieldsContainer.removeAttribute("data-show");
    }

    // метод збірки прізвища, імені та по-батькові
    updateFullName() {
        const fullName = `${this.lastNameInput.value} ${this.firstNameInput.value} ${this.patronymicInput.value}`;
        this.fullNameInput.value = fullName;
    }  // метод для збірки поля fullName - формуємо повне ім'я  

    // установити дізейбл на невалідних для певного лікаря полів
    disableNonRelevantFields() {
        const selectedDoctorType = this.choiceDoctor.value;
        this.fieldsContainers.forEach(container => {
            const containerId = container.id;
            if (containerId !== selectedDoctorType && containerId !== 'common_fields_container') {
                container.querySelectorAll("input, select, textarea").forEach(item => {
                    item.disabled = true;
                });
            }
        });
    }

    // Зняти дізейбл з усіх полів у всіх контейнерах
    removeAllDisabled() {
        this.fieldsContainers.forEach(container => {
            container.querySelectorAll("input, select, textarea").forEach(item => {
                item.disabled = false;
            });
        });
    }

    updateDefaultFields() {
        document.getElementById("modal-visit__firstName").value = "Michael";
        document.getElementById("modal-visit__lastName").value = "Jackson";
        document.getElementById("modal-visit__patronymic").value = "singer";
        document.getElementById("modal-visit__fullName").value = "Jackson Michael singer";
        document.getElementById("modal-visit__title").value = "Sing songs";
        document.getElementById("modal-visit__description").value = "Sing songs at a concert";
        document.getElementById("modal-visit__dropdownUrgency_dentist").value = "Normal";
    }
}
