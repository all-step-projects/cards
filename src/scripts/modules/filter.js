//Section search
let inputText = document.querySelector('.nav__search-arr');
let cardInfo;
let searchArr = document.querySelector('.search')

//Active search
inputText.oninput = function () {
    let value = this.value.trim().toLowerCase();
    let listCards = document.querySelectorAll('.card');
    listCards.forEach((card) => {
        if (card.innerText.toLowerCase().search(value) == -1) {
            card.classList.add('hidden');
        } else {
            card.classList.remove('hidden');
        }
    });
}

// Search by selected option
searchArr.addEventListener('click', (event) => {
    event.preventDefault();
    let searchStatus = searchArr.querySelector('.status_current');
    let priorityCurrent = searchArr.querySelector('.priority_current');
    cardInfo = document.querySelectorAll('.card');

    cardInfo.forEach((element) => {
        element.classList.remove('hidden');
        let cardBase = element.querySelectorAll('.card__base');
        cardBase.forEach((cardItem) => {

            let statusItem = cardItem.textContent.toLowerCase().indexOf(`${searchStatus.textContent}`);
            let priorityItem = cardItem.textContent.toLowerCase().indexOf(`${priorityCurrent.textContent}`);

            if (searchStatus.textContent !== 'all' && priorityCurrent.textContent === 'all') {
                if (statusItem === -1) {
                    element.classList.add('hidden');
                }
            } else if (searchStatus.textContent === 'all' && priorityCurrent.textContent !== 'all') {
                if (priorityItem === -1) {
                    element.classList.add('hidden');
                }
            } else if (searchStatus.textContent !== 'all' && priorityCurrent.textContent !== 'all') {
                if (statusItem === -1 || priorityItem === -1) {
                    element.classList.add('hidden');
                }
            } else if (searchStatus.textContent === 'all' && priorityCurrent.textContent === 'all') {
                element.classList.remove('hidden');
            }
        });
    });
});