import { sendRequest } from "../functions/sendRequest.js";
import { URL, TOKEN } from "../configs/consts.js";
import {
  CONFIG_GET_ALL,
  CONFIG_POST,
  CONFIG_PUT,
  CONFIG_DEL,
} from "../configs/config.js";

import { Cards } from "./create_cards.js";
import { ModalVisitFormRender } from "./visit_modal.js";

// DATABASE interaction and maintenance
class DataService {
  constructor() {
    this.renderForm = new ModalVisitFormRender(); // Відкриває модальне вікно форми для заповнення

    this.form = document.querySelector("#modal-visit__create"); // Стукаємося до форми самой форми в HTML
    this.cardsBoard = document.querySelector("#board_cards"); // дошка у HTML, куди закидуються усі картки
    this.btnCreateCard = document.querySelector("#btn_submit_form"); // Стукаємося до кнопки "Create Card" (задля подальшого її приховування)
    this.btnEditForm = document.querySelector("#btn_edit_form"); // кнопка завершення редагування форми, та відсилання запути на зміну даних у базі

    this.init();
    this.createCard();
    this.callEditForm();
    this.deleteCard();
  }

  // Here collect all methods async that need to be initiated or run
  async init() {
    await this.getDataBase();
    await this.addEditFormListener();
  }

  // Get all exist cards from database
  async getDataBase() {
    try {
      const allDataBase = await sendRequest(URL, "GET", CONFIG_GET_ALL);
      console.log("Exist database: ", allDataBase);

      allDataBase.forEach((item) => {
        Cards.getCardData(item); // ця функція отримує існуючі значення з бази даних і наповнює їми усі необхідні глобальні this

        Cards.createCardElement(); // потім запускаємо функцію-шаблон, яка відповідає за тегі, вунтрішнього наповення, та візуальний вигляд картки. Вона ствоює самі картки, та наповнює значеннями взятими в this.
      });
    } catch (error) {
      console.error("Some mistake:", error);
    }
  }

  //The function for create cards
  async createCard() {
    this.form.addEventListener("submit", (event) => this.formSubmit(event)); // встановлюємо прослуховувач на форму. Коли спрацьовує натискання на кнопку "submit"
  }

  //The function for harvest data from form, submit and POST data to the data server
  async formSubmit(event) {
    event.preventDefault();
    const formData = new FormData(this.form); // FormData – об'єкт у який збирається дані з полів форми

    try {
      const response = await sendRequest(URL, "POST", CONFIG_POST(formData));
      console.log(response);
      Cards.getCardData(response); // це функція перехоплює значення з форми, які спрямовані да бази даних, і наповнює їми усі необхідні глобальні this
      Cards.createCardElement(); // потім запускаємо функцію-шаблон, яка відповідає за тегі, вунтрішнього наповення, та візуальний вигляд картки. Вона ствоює самі картки, та наповнює значеннями взятими в this
    } catch (error) {
      console.error("Some mistake:", error);
    }
  }

  // The function of edit an existing card
  callEditForm() {
    this.cardsBoard.addEventListener("click", async (event) => {
      event.preventDefault();

      this.btnEditCard = event.target.closest(".btn_edit_card"); // Get a "Edit" button for edit a card
      if (!this.btnEditCard) return;

      this.findEditCard = this.btnEditCard.closest(".card"); // Find that card that was pressed button "Edit"
      this.getIdCard = this.findEditCard.dataset.idCard; // Get the unique ID of a card for deletion card from the database

      this.findSpecialist = this.findEditCard.querySelector(
        ".card_specialist_id"
      );

      const specialistValue = this.findSpecialist.textContent.toLowerCase();
      console.log(specialistValue);

      this.renderForm.openModalEditVisitFormWithSelectedDoctor(specialistValue); //відкриття модального окна форми відповідно лікарю, передаємо лікаря

      try {
        const cardData = await this.getExistingCardDataFromServer(this.getIdCard);        
        this.populateFormFields(cardData); // метод, щоб наповнити поля форми отриманими даними нашої картки для подальшого редагування        
      } catch (error) {
        console.error("Error calling edit form:", error);
      }

      this.swichBtnEditVsCreate();
    });
  }

  async getExistingCardDataFromServer(getIdCard) {
    try {
      const cardData = await sendRequest(`${URL}/${getIdCard}`, "GET", CONFIG_GET_ALL);
      console.log("Отримали об'єкт картки для подальшого редагування: ", cardData);
      return cardData;
    } catch (error) {
      console.error("Error fetching card data:", error);
      throw error;
    }
  }

  populateFormFields(cardData) {
    document.getElementById("modal-visit__lastName").value = cardData.lastName;
    document.getElementById("modal-visit__firstName").value = cardData.firstName;
    document.getElementById("modal-visit__patronymic").value = cardData.patronymic;
    document.getElementById("modal-visit__fullName").value = cardData.fullName;
    document.getElementById("modal-visit__title").value = cardData.title;
    document.getElementById("modal-visit__description").value = cardData.description;
    document.getElementById("modal-visit__dateLastVisit").value = cardData.dateLastVisit;
    document.getElementById("modal-visit__dropdownUrgency").value = cardData.dropdownUrgency;
    document.getElementById("modal-visit__dropdownUrgency_dentist").value = cardData.dropdownUrgency;
    document.getElementById("modal-visit__dropdownUrgency-therapist").value = cardData.dropdownUrgency;
    document.getElementById("modal-visit__status").value = cardData.status;
    document.getElementById("modal-visit__status-dentist").value = cardData.status;
    document.getElementById("modal-visit__status-therapist").value = cardData.status;
    document.getElementById("modal-visit__normalPressure").value = cardData.normalPressure;
    document.getElementById("modal-visit__age").value = cardData.age;
    document.getElementById("modal-visit__age-therapist").value = cardData.age;
    document.getElementById("bodyWeight").value = cardData.bodyWeight;
    document.getElementById("bodyHeight").value = cardData.bodyHeight;
    document.getElementById("modal-visit__bodyMassIndex").value = cardData.bodyMassIndex;
    document.getElementById("modal-visit__transferredCardiacDiseases").value = cardData.transferredCardiacDiseases;
  }

  async addEditFormListener() {
    // At the end, let's show button "Creade Card" and hide button "Edit Card"
    this.btnEditForm.addEventListener("click", async (event) => {
      event.preventDefault();
      this.form = document.querySelector("#modal-visit__create"); // Стукаємося до форми
      this.formDataEdit = new FormData(this.form); // FormData – об'єкт у який збирається дані з полів форми

      // console.log(this.formDataEdit);
      try {
        const response = await sendRequest(
          `${URL}/${this.getIdCard}`,
          "PUT",
          CONFIG_PUT(this.formDataEdit)
        );
        const jsonResponse = await response.json();
        console.log(jsonResponse);
        Cards.updateCardElement(this.getIdCard, jsonResponse);
      } catch (error) {
        console.error("Mistake with the PUT method:", error);
      }

      this.closeEditForm(); // Closes the form after to press the Edit Card button
    });
  }

  // The function that turns off the Create Card button and temporary turns on the Edit Card button, only to Edit Form
  swichBtnEditVsCreate() {
    this.btnCreateCard.style.display = "none";
    this.btnEditForm.style.display = "block";
  }

  // The function returns the Create Card button and turns off the Edit Card button
  closeEditForm() {
    this.btnCreateCard.style.display = "block";
    this.btnEditForm.style.display = "none";
    this.renderForm.closeModalCreateVisitForm();
  }

  // The function of removing one existing card from everywhere
  async deleteCard() {
    try {
      this.cardsBoard.addEventListener("click", (event) => {
        const btnDelCard = event.target.closest(".btn_delete_card"); // Get button "X" for delete a card
        if (!btnDelCard) return;

        const getCard = btnDelCard.closest(".card"); // Get the card container
        const getIdCard = getCard.dataset.idCard; // Get the unique Id of a card for deletion card from the database

        sendRequest(`${URL}/${getIdCard}`, "DELETE", CONFIG_DEL); // Delete card from database (fetch request)

        getCard.remove(); // Delete a card from HTML

        Cards.switchTextEmptyCards(); // Call switchTextEmptyCards() for checking the last card was deleted
      });
    } catch (error) {
      console.error("Some mistake:", error);
    }
  }
}

// // The function switcher for debugging
// const dataService = new DataService();

// The function needs for Gleb! Don't remove!!!
export function startCards() {
  const dataService = new DataService();
}
