import { BODY } from "../configs/consts.js";
import { someMail } from "../configs/consts.js";
import { somePassword } from "../configs/consts.js";
import { TOKEN } from "../configs/consts.js";
import { URL } from "../configs/consts.js";
import { startCards } from "./data_service.js";

export let responseToken;
let enterswich = 0;
const btnSignIn = document.querySelector("#btn_sign_in");
const btnVisit = document.querySelector("#btn_create_visit");
const btnExit = document.querySelector("#btn_exit");

export class Entrance {
  constructor() {
    // ANDRII: створюю конструкор, щоб уникнути дюблювання коду в різних методах

    this.getInnerStyles = document.querySelectorAll(".inner"); // ANDRII: get all inner-styles
    this.getSearchElements = document.querySelectorAll(".search_style"); // ANDRII: get search elements from Navigation bar
  }

  createModalEntrance() {
    btnSignIn.addEventListener("click", () => {
      const findEntranseModule = document.querySelector(".entrance"); //Check if module exist
      if (findEntranseModule === null) {
        const createGeneralEntry = document.createElement("div");
        createGeneralEntry.classList.add("entrance", "border", "shadow");

        createGeneralEntry.insertAdjacentHTML(
          "beforeend",
          `
            <div>
              <button class="btn__close modal_entrence_close">X</button>
              <form class="entrance__form user_data">
                <input
                  type="email"
                  class="entrance__logo user_email"
                  placeholder=${someMail}
                  title="email"
                  autofocus="logo"
                  name="logo"
                />
                <input
                  type="password"
                  class="btn-general btn-field user_pass"
                  placeholder=${somePassword}
                  title="password"
                  name="password"
                />
                <input type="reset" value="Sign In" class="btn usersign_btn" />
              </form>
            </div>

            `
        );

        BODY.append(createGeneralEntry);
      }
      this.checkUserData(); //Chech email and pass -> token

      this.deleteModalSign(); //Delete modal_entrace  by btn__close (X) and buttun "Escape"

      this.createCloakMode(); // ANDRII: Set Cloak-mode Style for Global document by pressed Sign In button
    });

    btnExit.addEventListener("click", (event) => {
      btnExit.classList.toggle("hidden");
      btnSignIn.classList.toggle("hidden");
      const currentCards = document.querySelectorAll(".card");
      currentCards.forEach((element) => {
        element.remove();
        responseToken = "";
        enterswich = 0;
        btnVisit.classList.add("hidden");
      });
    });
  }

  // Gleb: Delete modal_entrace  by btn__close (X) and buttun "Escape"
  deleteModalSign() {
    const btnClose = document.querySelector(".modal_entrence_close");
    const modalEntrance = document.querySelector(".entrance");

    BODY.addEventListener("keyup", (event) => {
      if (event.code === "Escape") {
        modalEntrance.remove();
        this.removeCloakMode(); // ANDRII: remove style from global cloak
      }
    });

    btnClose.addEventListener("click", (event) => {
      modalEntrance.remove();
      this.removeCloakMode(); // ANDRII: remove style from global cloak
    });

    if (responseToken === TOKEN && !enterswich) {
      // alert('OK');
      modalEntrance.remove();
      startCards();
      btnSignIn.classList.toggle("hidden");
      btnExit.classList.toggle("hidden");
      btnVisit.classList.remove("hidden");

      enterswich = 1;
      this.removeCloakMode(); // ANDRII: remove style from global cloak
    }
  }

  checkUserData() {
    const userSignBtn = document.querySelector(".usersign_btn");
    userSignBtn.addEventListener("click", (event) => {
      if (responseToken !== TOKEN) {
        console.log("Go");
        this.getToken(someMail, somePassword);
      }
    });
  }

  // Get token
  getToken(email, passwor) {
    fetch(`${URL}/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: "glebvoroniy@gmail.com",
        password: "ajax.gleb2024",
      }),
    })
      .then((response) => response.text())
      .then((token) => {
        responseToken = token;
        if (responseToken === TOKEN) {
          entrance.deleteModalSign();
        }
      });
  }

  // ANDRII: Create style for Global Cloak-mode
  createCloakMode() {
    this.getInnerStyles.forEach((element) => {
      element.style.backgroundColor = `#ced7df`;
      element.style.opacity = "0.50";
    });

    this.getSearchElements.forEach((element) => {
      element.style.backgroundColor = `#ced7df`;
      element.style.opacity = "0.50";
    });
  }

  // ANDRII: Ucloak-mode style from global body
  removeCloakMode() {
    this.getInnerStyles.forEach((element) => {
      element.style.backgroundColor = "";
      element.style.opacity = "1";
    });

    this.getSearchElements.forEach((element) => {
      element.style.backgroundColor = "";
      element.style.opacity = "1";
    });
  }
}

export const entrance = new Entrance();
