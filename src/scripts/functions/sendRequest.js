export async function sendRequest(url, method = "GET", config = {}) {
  try {
    const response = await fetch(url, { method, ...config });

    if (!response.ok) throw new Error(`Fail to fetch from ${url}`);
    console.log(response);

    if (method === "DELETE" || method === "PUT") {
      return response;
    } else {
      return response.json();
    }
  } catch (error) {
    console.log("Error fetch data: ", error);
    throw error;
  }
}
